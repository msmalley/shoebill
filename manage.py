#!/usr/bin/env python
import shoebill
import os.path
from shoebill.api import ShoebillController


def create_app(args):
    from flask import Flask

    shoebill_dir = os.path.dirname(shoebill.__file__)
    static_dir = os.path.join(shoebill_dir, 'static')
    template_dir = os.path.join(shoebill_dir, 'templates')

    return Flask('shoebill', static_folder=static_dir, template_folder=template_dir )


def create_controller(args):
    app = create_app(args)
    cfg = {}

    if os.path.exists(args.config):
        execfile(args.config, cfg)
    else:
        cfg = shoebill.DEFAULT_CONFIG

    for (k, v) in cfg.get('flask', {}).items():
        setattr(app, k, v)

    return ShoebillController(app, **cfg)


class Commands(object):
    @staticmethod
    def init(args):
        skel = os.path.join(os.path.dirname(__file__), 'skel')
        from shoebill.util import sync_dir
        sync_dir(skel, args.dir)
        print args.dir + ' is now initialized'

    @staticmethod
    def console(args):
        controller = create_controller(args)
        reload = args.debug and not args.noreload
        controller.app().run(debug=args.debug, use_reloader=reload)

    @staticmethod
    def publish(args):
        from shoebill.api import StaticSite
        controller = create_controller(args)
        reload = args.debug and not args.noreload
        controller.app().run(debug=args.debug, use_reloader=reload)

        published = controller.find(StaticSite).publish_site(args.republish)

        print str(len(published)) + ' files published'

        from flask import request
        shutdown = request.environ.get('werkzeug.server.shutdown')
        if shutdown is None:
            print "not running Werkzeug, don't know how to cleanly shutdown"
        else:
            shutdown()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='cmd')

    init = subparsers.add_parser('init')
    init.add_argument('-d', '--dir', default='.')

    console = subparsers.add_parser('console')
    console.add_argument('-c', '--config', default='./config.py')
    console.add_argument('-d', '--debug', action='store_true')
    console.add_argument('-R', '--noreload', action='store_true')

    publish = subparsers.add_parser('publish', parents=[console], add_help=False)
    publish.add_argument('-r', '--republish', action='store_true')

    args = parser.parse_args()
    getattr(Commands, args.cmd, None)(args)