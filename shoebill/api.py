from .domain import Post, Page, DATE_FORMAT
from .features import Feature
from .flask_views import route, View, Controller, ConfigProvider
from .util import outdated, sync_dir, AttrDict
from datetime import datetime
from flask import request, session, render_template, redirect, url_for, send_from_directory
import markdown
import os
import os.path
import re
import unicodedata

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO


def slugify(value):
    """
    Converts to lowercase, removes non-word characters (alphanumerics and
    underscores) and converts spaces to hyphens. Also strips leading and
    trailing whitespace.

    Stolen (graciously) from django 1.7
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)


def menu_item(name, endpoint, endpoint_args={}, priority=10):
    return dict(name=name, endpoint=endpoint, endpoint_args=endpoint_args, priority=priority)


def sidebar_item(title, content, template='sidebar_item.html'):
    return dict(title=title, content=content, template=template)


class ShoebillView(View):
    menu_items = []

    def is_menu_item_active(self, url, endpoint):
        return request.path.startswith(url)

    def template_context(self, **kwargs):
        ctx = dict(
            menu_items=self.find(ShoebillController).menu_items,
            is_menu_item_active=self.is_menu_item_active
        )

        if 'message' in session:
            ctx['message'] = session['message']
            del session['message']

        if 'messages' in session:
            ctx['messages'] = session['messages']
            del session['messages']

        if 'error' in session:
            ctx['error'] = session['error']
            del session['error']

        if 'errors' in session:
            ctx['errors'] = session['errors']
            del session['errors']

        ctx.update(kwargs)
        return ctx


class Backend(Feature):
    def get_objects(self, **kwargs):
        raise NotImplementedError

    def get_object(self, id, **kwargs):
        raise NotImplementedError

    def save_object(self, obj, **kwargs):
        raise NotImplementedError

    def delete_object(self, obj, **kwargs):
        raise NotImplementedError


class Markdown(Feature):
    def get_converter(self, parse_meta=True):
        extensions = ['codehilite', 'fenced_code']

        if parse_meta:
            extensions.append('meta')

        return markdown.Markdown(extensions=extensions)

    def convert(self, source, parse_meta=False):
        md = self.get_converter(parse_meta)
        return md.convert(source)

    def convertFile(self, input, output, parse_meta=False):
        md = self.get_converter(parse_meta)
        return md.convertFile(input, output)


class FileSystemBackend(Backend):
    requires = [Markdown]

    @property
    def content_dir(self):
        return os.path.join(self.find(ConfigProvider).config().base_dir, 'content')

    def get_objects(self, include=lambda o: True, exclude=lambda o: False, **kwargs):
        root = kwargs.get('directory', self.content_dir)
        for (dirpath, dirnames, filenames) in os.walk(root):
            for filename in filenames:
                (name, ext) = os.path.splitext(filename)

                if ext != '.md':
                    continue

                relpath = os.path.join(os.path.relpath(dirpath, root), filename)
                obj = self.get_object(relpath)

                if include(obj) and not exclude(obj):
                    yield obj

    def get_object(self, id, **kwargs):
        parse_meta = kwargs.get('parse_meta', True)
        path = os.path.join(self.content_dir, id)
        meta = {
            'id': id,
            'type': 'post',
            'status': 'published',
            'created': datetime.fromtimestamp(os.path.getctime(path)),
            'modified': datetime.fromtimestamp(os.path.getmtime(path))
        }

        # originally i let the markdown 'meta' extension parse metadata
        # but ran into problems with it when combined with the codehilight
        # instead just doing a simple version of that here

        body = ''

        with open(path, 'r') as fp:
            import re
            in_meta = parse_meta

            for line in fp:
                if in_meta:
                    match = re.match('^(\w+):\s*(.*)', line)
                    in_meta = bool(match)

                    if line in ['\r', '\n', '\r\n']:
                        # skip first empty line after meta headers
                        continue

                    if match:
                        key = match.group(1)
                        val = match.group(2).strip()
                        meta[key] = val

                if not in_meta:
                    body += line

        html = self.find(Markdown).convert(body)

        return Post(body=body,
                    html=html,
                    **meta)

    def save_object(self, obj, orig=None, force=False, **kwargs):
        slug = slugify(obj.title)
        filename = slug + '.md'
        subdir = obj.type
        id = os.path.join(subdir, filename)
        path = os.path.join(self.content_dir, id)

        if os.path.exists(path) and not force:
            raise IOError(filename + ' exists')

        obj.id = id

        # convert
        obj.html = self.find(Markdown).convert(obj.body)

        # write source file

        dir = os.path.dirname(path)

        if not os.path.exists(dir):
            os.makedirs(dir)

        with open(path, 'w') as fp:
            for key, val in obj.items():
                if key in ['body', 'html', 'modified', 'created']:
                    continue
                elif key == 'date':
                    val = val.strftime(DATE_FORMAT)
                fp.write("%s: %s\n" % (key, str(val)))

            fp.write("\n")
            fp.write(obj.body)
            fp.write("\n")

        # cleanup old files if renamed

        if orig is not None:
            orig_subdir = 'posts'

            if obj.type == 'page':
                orig_subdir = 'pages'

            orig_slug = slugify(orig.title)
            orig_filename = orig_slug + '.md'

            if orig_filename != filename:
                path = os.path.join(self.content_dir, orig_subdir, orig_filename)
                os.unlink(path)

                os.unlink(os.path.join(self.site_dir, orig_filename))


class StaticSite(Feature):
    requires = [FileSystemBackend]

    class Publisher(Feature):
        def build_env(self, env):
            pass

        def publish(self, env, published):
            pass

    def __init__(self, **kwargs):
        super(StaticSite, self).__init__(**kwargs)
        from jinja2 import Environment, FileSystemLoader
        self.jinja = Environment(loader=FileSystemLoader(self.theme_dir))

    @staticmethod
    def site_url(filename):
        if filename.startswith('/'):
            filename = filename[1:]
        url = url_for('Site:static_file', filename=filename)
        return url

    @property
    def config(self):
        return self.find(ConfigProvider).config()

    @property
    def base_dir(self):
        return self.config.base_dir

    @property
    def site_dir(self):
        return os.path.join(self.base_dir, 'site')

    @property
    def theme_dir(self):
        return os.path.join(self.base_dir, 'theme')

    def render(self, template_path, ctx):
        template = self.jinja.get_template(template_path)
        return template.render(ctx)

    def render_to_file(self, template_path, path, ctx):
        template = self.jinja.get_template(template_path)
        stream = template.stream(ctx)

        with open(path, 'w') as fp:
            stream.dump(fp)

    def site_context(self, **kwargs):
        ctx = dict(now=datetime.now(), site_url=self.site_url, **self.config)
        ctx.update(kwargs)
        return ctx

    def publish_site(self, republish=False):
        # init site dir

        if not os.path.exists(self.site_dir):
            os.makedirs(self.site_dir)

        # copy static files for theme

        published = []
        static_dir = os.path.join(self.theme_dir, 'static')

        if os.path.exists(static_dir):
            dst_dir = os.path.join(self.site_dir, 'static')
            published += sync_dir(static_dir, dst_dir, republish)

        env = AttrDict(jinja=self.jinja, force=republish, sidebar_items=[])

        for publisher in self.find_all(StaticSite.Publisher):
            publisher.build_env(env)

        for publisher in self.find_all(StaticSite.Publisher):
            publisher.publish(env, published)

        # publish site index

        src = os.path.join(self.theme_dir, 'index.html')
        dst = os.path.join(self.site_dir, 'index.html')

        if published or republish or outdated(src, dst):
            env.setdefault('posts', [])
            env.posts = sorted(filter(lambda p: p.status == 'published', env.posts), key=lambda p: p.date, reverse=True)
            env.setdefault('pages', [])

            ctx = self.site_context(**env)
            self.render_to_file('index.html', dst, ctx)
            published.append('index.html')

        return published


class Index(ShoebillView):
    @route('/')
    def index(self):
        return redirect(url_for('Posts:posts'))


class About(ShoebillView, StaticSite.Publisher):
    requires = [ConfigProvider]

    def build_env(self, env):
        about = self.find(ConfigProvider).config().about
        if about:
            env.sidebar_items.append(sidebar_item('About', about))


class Posts(ShoebillView, StaticSite.Publisher):
    _requires = [Backend, Markdown]
    menu_items = [
        menu_item(name='Posts', endpoint='Posts:posts', priority=1),
        menu_item(name='Compose', endpoint='Posts:compose')]

    @route('/preview', methods=['POST'])
    def preview(self):
        source = request.form['source']
        html = self.find(Markdown).convert(source)
        return html

    @route('/posts')
    def posts(self):
        posts = self.find(Backend).get_objects(include=lambda o: o.type == 'post')
        posts = sorted(posts, key=lambda p: p.date, reverse=True)
        ctx = self.template_context(posts=posts)
        return render_template('posts.html', **ctx)

    @route('/compose', methods=['GET', 'POST'])
    def compose(self):
        post = Post()

        if request.method == 'POST':
            ctx = self.save_post(post, request.form)
        else:
            ctx = self.template_context(post=post)

        return render_template('compose.html', **ctx)

    @route('/posts/edit', methods=['GET', 'POST'])
    def edit(self):
        id = request.args.get('id', '')

        if not id:
            session['error'] = 'id is required'
            return redirect(url_for('Posts:posts'))

        post = self.find(Backend).get_object(id)

        if not post:
            session['error'] = id + ' not found'
            return redirect(url_for('Posts:posts'))

        if request.method == 'POST':
            ctx = self.save_post(post, request.form, force=True)
        else:
            ctx = self.template_context(post=post)

        return render_template('edit.html', **ctx)

    def save_post(self, post, form, force=False):
        orig = None

        if post is None:
            post = Post()

        if 'id' in form:
            orig = self.controller.posts.from_file(form['id'])

        # type

        post.type = form.get('type', default='post')

        # status (is draft?)

        if form.get('is_draft', default=''):
            post.status = 'draft'
        else:
            post.status = 'published'

        # title

        post.title = form['title']

        # date

        if 'date' in form:
            post.date = datetime.strptime(form.get('date'), DATE_FORMAT)

        # misc

        post.summary = form.get('summary', default=None)
        post.tags = form.get('tags', default='')
        post.body = form.get('body', default='')

        # fix line endings; apparently the browser always submits \r\n from textarea

        if os.linesep != '\r\n':
            post.body = post.body.replace('\r\n',os.linesep)

        # create ctx and save

        ctx = self.template_context(post=post)

        try:
            self.find(Backend).save_object(post, orig=orig, force=force)
            ctx['message'] = '"%s" saved' % (post.title,)
        except IOError, e:
            ctx['error'] = str(e)

        return ctx

    def publish(self, env, published):
        env.posts = sorted(self.find(Backend).get_objects(lambda o: o.type == 'post'), key=lambda p: p.date)

        for post in env.posts:
            if post.status == 'draft':
                continue

            site = self.find(StaticSite)

            dst_filename = os.path.splitext(post.id)[0] + '.html'
            dst_path = os.path.join(site.site_dir, dst_filename)

            post.url = site.site_url(dst_filename)
            missing = not os.path.exists(dst_path)
            outdated = missing or post.modified > datetime.fromtimestamp(os.path.getmtime(dst_path))

            if env.force or missing or outdated:
                dir = os.path.dirname(dst_path)

                if not os.path.exists(dir):
                    os.makedirs(dir)

                ctx = site.site_context(**env)
                ctx.update(post)

                site.render_to_file('post.html', dst_path, ctx)
                published.append(post.id)


class Pages(ShoebillView):
    menu_items = [menu_item(name='Pages', endpoint='Pages:pages', priority=2)]

    @route('/pages')
    def pages(self):
        pages = self.find(Backend).get_objects(include=lambda o: o.type == 'page')
        ctx = self.template_context(pages=pages)
        return render_template('pages.html', **ctx)

    @route('/page', methods=['GET', 'POST'])
    def page(self):
        page = Page(title='dummy')
        ctx = self.template_context(**page)
        return render_template('page.html', **ctx)


class Site(ShoebillView):
    menu_items = [
        menu_item(name='Publish', endpoint='Site:publish', priority=98),
        menu_item(
            name='View Site',
            priority=99,
            endpoint='Site:static_file',
            endpoint_args=dict(filename='index.html'))
    ]

    @route('/site/<path:filename>')
    def static_file(self, filename):
        return send_from_directory(self.find(StaticSite).site_dir, filename)

    @route('/publish', methods=['GET', 'POST'])
    def publish(self):
        ctx = self.template_context()

        if request.method == 'POST':
            republish = bool(request.form.get('republish', default=''))
            published = self.find(StaticSite).publish_site(republish)
            ctx['message'] = str(len(published)) + ' files published'

        return render_template('publish.html', **ctx)


class ShoebillController(Controller):
    view_classes = [Markdown, FileSystemBackend, StaticSite, Index, Posts, Pages, Site, About]

    def __init__(self, app, **config):
        self.menu_items = []
        super(ShoebillController, self).__init__(app, **config)

    def add_view(self, view_or_cls):
        view = super(ShoebillController, self).add_view(view_or_cls)

        if hasattr(view, 'menu_items'):
            self.menu_items += view.menu_items