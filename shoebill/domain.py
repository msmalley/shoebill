from .util import AttrDict
from datetime import datetime

DATE_FORMAT = '%Y-%m-%d %H:%M'
EPOCH = datetime.fromtimestamp(0)


class Post(AttrDict):
    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)

        # special case for date: set a default value or parse into datetime instance

        if 'date' not in self:
            self.date = datetime.now()
        elif isinstance(self.date, basestring):
            self.date = datetime.strptime(self.date, DATE_FORMAT)

    def __setitem__(self, key, value):
        # special case for date to parse into datetime instance
        # NOTE: __setitem__ not invoked by __init__; special date handling required there as well
        if key == 'date' and isinstance(value, basestring):
            value = datetime.strptime(value, DATE_FORMAT)

        super(Post, self).__setitem__(key, value)


class Page(Post):
    pass