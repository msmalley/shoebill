from features import Feature, FeatureSet
from .util import Fn, SafeAttrDict


def route(rule, **options):
    """route decorator for Plugin that will support bound methods"""
    def decorator(fn):
        endpoint = options.pop('endpoint', None)
        _route = dict(rule=rule, options=options)

        if endpoint:
            _route['endpoint'] = endpoint

        setattr(fn, '_route', _route)
        return fn
    return decorator


class View(Feature):
    def __init__(self, **kwargs):
        Feature.__init__(self, **kwargs)

        for name in self.__class__.__dict__:
            item = getattr(self, name)

            # check for route info

            if hasattr(item, '_route'):
                # get route info and add url rule
                # NOTE: assuming item is bound method

                _route = getattr(item, '_route')
                view_fn = Fn(item)  # GOTCHA: def statement won't work inside for loop; wrap w/ callable instead

                rule = _route['rule']
                endpoint = _route.get('endpoint', self.view_name + ':' + name)
                options = _route.get('options', {})

                self.app.add_url_rule(rule, endpoint, view_func=view_fn, **options)

    @property
    def view_name(self):
        return self.__class__.__name__

    @property
    def app(self):
        return self.find(AppProvider).app()

    @property
    def config(self):
        return self.find(ConfigProvider).config()


class AppProvider(Feature):
    def __init__(self, app, **kwargs):
        Feature.__init__(self, **kwargs)
        self._app = app

    def app(self):
        return self._app


class ConfigProvider(Feature):
    def __init__(self, config, **kwargs):
        Feature.__init__(self, **kwargs)
        self._config = SafeAttrDict(**config)

    def config(self):
        return self._config


class ViewProvider(Feature):
    def __init__(self, view_classes=[], **kwargs):
        Feature.__init__(self, **kwargs)
        self.views = []

        for cls in view_classes:
            self.add_view(cls)

    def add_view(self, view_or_cls):
        if isinstance(view_or_cls, View):
            view = view_or_cls
        else:
            view = view_or_cls(feature_sets=self.feature_sets)

        self.views.append(view)
        return view


class Controller(FeatureSet, AppProvider, ConfigProvider, ViewProvider):
    view_classes = []

    def __init__(self, app, **config):
        FeatureSet.__init__(self)
        Feature.__init__(self, feature_set=self)
        AppProvider.__init__(self, app)
        ConfigProvider.__init__(self, SafeAttrDict.make_safe(config))
        ViewProvider.__init__(self, self.view_classes)