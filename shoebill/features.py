
def provides(cls_or_obj):
    if isinstance(cls_or_obj, type):
        cls = cls_or_obj
    else:
        cls = cls_or_obj.__class__

    return [base for base in cls.__mro__ if issubclass(base, Feature)]


def feature_name(obj):
    return getattr(obj, 'feature_name', obj.__class__.__name__)


class FeatureSet(object):
    def __init__(self, *args):
        self.features = set()

        for arg in args:
            self.add_feature(arg)

    def add_feature(self, feature_or_cls):
        if isinstance(feature_or_cls, Feature):
            feature = feature_or_cls
        else:
            feature = feature_or_cls()

        if feature in self.features:
            return feature

        requires = set(getattr(feature, 'requires', set()))

        if requires:
            available = {p for f in self.features for p in provides(f)}
            unresolved = requires - available

            if unresolved:
                raise ValueError(feature_name(feature) + ' has unresolved dependencies: ' + str(list(unresolved)))

        if hasattr(feature, 'feature_sets'):
            feature.feature_sets.append(self)

        self.features.add(feature)
        return feature

    def remove_feature(self, feature):
        if feature in self.features:
            self.features.remove(feature)
        if hasattr(feature, 'feature_sets') and self in feature.feature_sets:
            feature.feature_sets.remove(self)

    def find_all(self, name_or_cls):
        if isinstance(name_or_cls, basestring):
            test = lambda f: feature_name(f) == name_or_cls
        else:
            test = lambda f: isinstance(f, name_or_cls)

        return {f for f in self.features if test(f)}

    def find(self, name_or_cls):
        try:
            return next(iter(self.find_all(name_or_cls)))
        except StopIteration:
            return None


class Feature(object):
    def __init__(self, name=None, feature_set=None, feature_sets=None, **kwargs):
        self.feature_name = name if name else feature_name(self)

        if not hasattr(self, 'feature_sets'):
            self.feature_sets = []

        if feature_sets:
            for feature_set in feature_sets:
                feature_set.add_feature(self)

        if feature_set:
            feature_set.add_feature(self)

        cls_feature_set = getattr(self.__class__, 'feature_set', None)

        if cls_feature_set:
            cls_feature_set.add_feature(self)

    def __str__(self):
        return self.feature_name

    def find_all(self, name_or_cls):
        return {f for s in self.feature_sets for f in s.find_all(name_or_cls)}

    def find(self, name_or_cls):
        try:
            return next(iter(self.find_all(name_or_cls)))
        except StopIteration:
            return None

