from features import FeatureSet, Feature
import unittest


class MessageFormatter(Feature):
    def format(self, msg):
        raise NotImplemented


class TitleFormatter(MessageFormatter):
    def format(self, msg):
        return msg.title()


class SlugFormatter(MessageFormatter):
    def format(self, msg):
        return msg.lower().replace(' ', '-').strip("~`!@#$%^&*()_-+={}[]|\\;:'\"<>,.?/")


class MessageService(Feature):
    _requires = [MessageFormatter]

    def __init__(self, msg):
        super(MessageService, self).__init__()
        self.msg = msg

    def get_message(self):
        return self.find(MessageFormatter).format(self.msg)


class FeatureCoreTest(unittest.TestCase):
    def test_featureSet(self):
        msg = 'Lorem ipsum dolor...'
        title = TitleFormatter().format(msg)
        slug = SlugFormatter().format(msg)

        feature_set = FeatureSet(TitleFormatter(), MessageService(msg))

        self.assertEquals(len(feature_set.features), 2)
        self.assertEquals(feature_set.find(MessageService).get_message(), title)

        feature_set = FeatureSet(SlugFormatter(), MessageService(msg))
        self.assertEquals(len(feature_set.features), 2)
        self.assertEquals(feature_set.find(MessageService).get_message(), slug)

        feature_set.add_feature(TitleFormatter())

        self.assertEquals(len(feature_set.features), 3)
        formatted = {f.format(msg) for f in feature_set.find_all(MessageFormatter)}
        self.assertEquals(formatted, {title, slug})
