import os
import os.path
import re
import shutil
import unicodedata


class Fn(object):
    def __init__(self, fn, *default_args, **default_kwargs):
        self.fn = fn
        self.default_args = default_args
        self.default_kwargs = default_kwargs

    def __call__(self, *args, **kwargs):
        args = self.default_args + args
        kwargs = dict(**kwargs)
        kwargs.update(**self.default_kwargs)
        return self.fn(*args, **kwargs)


class AttrDict(dict):
    """dict extension that allows for attribute access of items

    Courtesy of Stack Overflow/Kimvais: http://stackoverflow.com/a/14620633/1458
    """
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


class SafeAttrDict(AttrDict):
    """returns None for missing keys instead of raising KeyError"""
    def __getitem__(self, key):
        if key in self:
            return super(SafeAttrDict, self).__getitem__(key)
        return None

    def __delitem__(self, key):
        if key in self:
            super(SafeAttrDict, self).__delitem__(key)

    @classmethod
    def make_safe(cls, val):
        if isinstance(val, dict) and not isinstance(val, SafeAttrDict):
            safe = SafeAttrDict()
            safe.update({k: SafeAttrDict.make_safe(v) for (k, v) in val.items()})
            return safe
        return val


def setdefaults(d, **defaults):
    for key in defaults:
        d.setdefault(key, defaults[key])


def outdated(src, dst):
    if not os.path.exists(src):
        return False

    return not os.path.exists(dst) or os.path.getmtime(src) > os.path.getmtime(dst)


def sync_dir(src_root, dst_root, force=False):
    """Sync files in one dir with another. By default only missing or outdated files will be copied

    Keyword arguments:

    force -- set to true if all files should be copied
    """
    files = []

    for (root, dirs, filenames) in os.walk(src_root):
        rel_dir = os.path.relpath(root, src_root)

        for filename in filenames:
            src = os.path.join(root, filename)
            dst = os.path.join(dst_root, rel_dir, filename)

            if force or outdated(src, dst):
                path = os.path.dirname(dst)

                if not os.path.exists(path):
                    os.makedirs(path)

                shutil.copyfile(src, dst)
                shutil.copystat(src, dst)
                files.append(dst)

    return files
