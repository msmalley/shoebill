# README

I've built this tool from my own needs. If you find it useful let me know! It takes a lot of inspiration from [pelican](http://blog.getpelican.com). It is very rough around the edges, alpha quality at best. YMMV.

## Try it out

```
#!bash
$ export PYTHONPATH=/path/to/repo
$ cd /path/to/my/blog
$ /path/to/repo/manage.py init
$ /path/to/repo/manage.py console -d
```

This will initialize your blog directory with a config file, theme, and example content, then run the console app on port 5000. Visit that page in your browser to view the content, compose new posts/pages, and publish the site.

There is a decent amount of work to do to make this usable. If you find it interesting please let me know as it will encourage me to invest more time in making it better! If you hate it that would also be useful to know but also make me sad :-(
